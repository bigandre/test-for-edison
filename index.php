<?php
  session_start();
  // Количество экстрасенсов
  $psychics_count = 2;

  // Числа, которые загадали экстрасенсы
  $new_attempt = array();

  // Инициализация массива достоверностей экстрасенсов
  if (!is_array($_SESSION['authenticity'])) $_SESSION['authenticity'] = array();

  // Если нет еще чисел, которые должны были загадать экстрасенсы
  $last_attempt = is_array($_SESSION['attempts']) ? end($_SESSION['attempts']) : array();
  if (!$last_attempt || $last_attempt['user_number']>0){
    for ($i=0;$i<$psychics_count;$i++) $new_attempt[$i]=rand(10,13);
    $_SESSION['attempts'][] = array('psychics_numbers'=>$new_attempt);
  }

  // Число, введенное пользователем, или 'undefine' - если число еще не введено
  $user_number = $_GET['number'];

  while (isset($_GET['number'])){
    // Проверка параметров ввода
    if (!is_numeric($user_number) || (0+$user_number) < 10 || (0+$user_number>99)){
      if (is_numeric($user_number))
        unset($_GET['number']);
      $user_number = '';
      break;
    }

    // Вытаскиваем элемент с последними загаданными экстрасенсами числами
    $last_attempt = array_pop($_SESSION['attempts']);

    // Устанавливаем введенное пользователем число, для статистики
    $last_attempt['user_number'] = 0+$user_number;

    // Инкрементирование уровня достоверности экстрасенса при необходимости
    for ($i=0;$i<$psychics_count;$i++){
      if ($last_attempt['psychics_numbers'][$i] == 0+$user_number)
        $_SESSION['authenticity'][$i] += 1; 
    }

    // Сохраняем данные для статистики
    $_SESSION['attempts'][] = $last_attempt;

    // Инициализируем новые числа, загаданные экстрасенсами
    for ($i=0;$i<$psychics_count;$i++) $new_attempt[$i]=rand(10,13);
    $last_attempt = array('psychics_numbers'=>$new_attempt);
    $_SESSION['attempts'][] = $last_attempt;
    break;
  }
?>
<!DOCTYPE html>
<html lang="ru">
<head>
</head>
<style>
table{
  border-spacing:0;
}
table td{
  border:1px solid gray;
  font-family:Arial;
  font-size:10px;
}
</style>
<body>
  <?php if(!isset($_GET['number'])):?>
    Загадайте двузначное число. Нажмите кнопку, чтобы получить догадки экстрасенсов
    <form action="." method="get">
      <input type="hidden" name="number" value="undefine"/>
      <input type="submit" value="Кнопка"/>
    </form>
  <?php else:?>
    <?php $i=0;foreach($last_attempt['psychics_numbers'] as $v):$i++;?>
      Экстрасенс <?=$i?>. Уровень достоверности <?=0+$_SESSION['authenticity'][$i-1]?>. Выбранное число: <?=$v?><br/>
    <?php endforeach;?>
    <form action="." method="get">
      Введите загаданное вами двузначное число:<br/>
      <input type="number" name="number" AUTOCOMPLETE="off" value="<?=$user_number?>"/>
      <input type="submit" value="Отправить"/>
    </form>
  <?php endif;?>
  <?php if (count($_SESSION['attempts'])>1):?>
    <br/>Статистика попыток:<br/>
    <table>
      <tr><td>Попытка</td><td>Число пользователя</td>
      <?php for ($i=1;$i<=$psychics_count;$i++):?>
         <td>Экстрасенс <?=$i?></td>
      <?php endfor;?>
      <?php for ($l = count($_SESSION['attempts'])-2;$l>=0;$l--):?>
      <tr>
        <td><?=$l+1?></td>
        <td><?=$_SESSION['attempts'][$l]['user_number']?></td>
        <?php for ($i=1;$i<=$psychics_count;$i++):?>
           <td><?=$_SESSION['attempts'][$l]['psychics_numbers'][$i-1]?></td>
        <?php endfor;?>
      </tr>
      <?php endfor;?>
      </tr>
    </table>
  <?php endif;?>
</body>
</html>
